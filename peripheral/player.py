import machine
import utime



# Buzzer on D0
d0 = machine.Pin('D0', machine.Pin.OUT)
# Buzzer on D1
d1 = machine.Pin('D1', machine.Pin.OUT)

def pitch (pin, noteFrequency, noteDuration, silence_ms = 10):
  if noteFrequency is not 0:
    microsecondsPerWave = 1e6 / noteFrequency
    millisecondsPerCycle = 1000 / (microsecondsPerWave * 2)
    loopTime = noteDuration * millisecondsPerCycle
    for x in range(loopTime):
      pin.high()
      utime.sleep_us(int(microsecondsPerWave))
      pin.low()
      utime.sleep_us(int(microsecondsPerWave))
  else:
    utime.sleep_ms(noteDuration)
  utime.sleep_ms(silence_ms)

def buzzer_playNotes (pin, notes, bpm = 120, ticks = 4):
  NOTE_FREQUENCIES = {
    'c': 16.352,
    'c#': 17.324, 'db': 17.324,
    'd': 18.354,
    'd#': 19.445, 'eb': 19.445,
    'e': 20.602,
    'f': 21.827,
    'f#': 23.125, 'gb': 23.125,
    'g': 24.500,
    'g#': 25.957, 'ab': 25.957,
    'a': 27.500,
    'a#': 29.135, 'bb': 29.135,
    'b': 30.868,
    'r': 0
  }
  for i in range(len(notes)):
    timeout = 60000 / bpm / ticks
    pin.off()
    n = notes[i].lower()
    data = n.split(':')
    note = 'r'
    octave = 4
    if len(data[0]) > 0:
      lastChar = data[0][-1]
      try:
        octave = int(lastChar)
        note = data[0].replace(lastChar, '')
      except:
        note = data[0]
    noteTicks = 1
    if len(data) > 1: noteTicks = int(data[1])
    n = {
      'note': note,
      'octave': octave,
      'ticks': noteTicks
    }
    n['f'] = NOTE_FREQUENCIES[n['note']]
    for o in range(n['octave']):
      n['f'] = n['f'] * 2
    pitch(pin, n['f'], timeout*n['ticks'])
music = "D-4:1D-4:1D-4:1G-4:4D#5:4S-1:2C#5:1C-5:1A#4:1A#5:4D#5:2S-1:1C#5:1C-5:1A#4:1A#5:4D#5:2C#5:1C-5:1C#5:1A#4:5"

chunked_list = list()
chunk_size = 3
for i in range(0, len(music), chunk_size):
    chunked_list.append(music[i:i+chunk_size])

buzzer_playNotes(d0, ['d', 'd', 'd'])
buzzer_playNotes(d0, ['g:4'])
buzzer_playNotes(d1, ['c1'])
