import math
import time
import random
import bluetooth
from ble_peripheral import DrivePeripheral


# -------------------------------
#             MAIN
# -------------------------------



def get_col(cases, pos):
    num = pos % 9
    return [cases[i] for i in range(num, 81, 9)]


def get_line(cases, pos):
    num = math.floor(pos / 9)
    return cases[num * 9:(num + 1) * 9]


def get_square(cases, pos):
    col = math.floor((pos % 9) / 3) * 3
    line = math.floor(math.floor(pos / 9) / 3) * 3
    res = []
    for j in range(col, col + 3):
        for i in range(line, line + 3):
            res.append(cases[i * 9 + j])
    return res


def get_all(cases, pos):
    return set(
        get_col(cases, pos) +
        get_line(cases, pos) +
        get_square(cases, pos)
    )


def get_possibles(cases,pos):
    return [x for x in "123456789" if x not in get_all(cases, pos)]



drv = DrivePeripheral(name='jawabot')
print("Peripheral up and running")
msg_len = 1
while True:
    msg = drv.get_message(recipient='ST')
    print(msg)
    # msg = drv.get_message(recipient='ST')
    data = msg.split(">")[1]
    cases = data.split(",")

    loop = True
    while loop:
        loop = False
        for j in range(0, len(cases)):
            if cases[j] == " ":
                possibilities = get_possibles(cases, j)
                if len(possibilities) == 1:
                    loop = True
                    cases[j] = possibilities[0]
    print(",".join(cases))
    drv.send_message(recipient='ST', msg=",".join(cases))
    # drv.send_message(recipient='ST', msg=randStr(msg_len))
