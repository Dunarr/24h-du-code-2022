import time

import machine
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_vl53l0x import VL53L0X
import utime
from stm32_ssd1306 import SSD1306, SSD1306_I2C

vl53l0x = VL53L0X(i2c=machine.I2C(1))
alphabot = AlphaBot_v2()
oled = SSD1306_I2C(128, 64, machine.I2C(1))

speed = 10

coefficient = [10, 2, 0, -2, -10]

whiteValue = [940.3714, 938.4164, 936.4614, 933.5288, 935.4838]

blackValue = [305.9628, 308.8954, 259.042, 210.1662, 256.1095]

'''
for i in range(0,5):
    input()
    print("bonjour je marche")
    blackValue.append(alphabot.TRSensors_readLine(sensor=0)[i])

print(blackValue)
print(whiteValue)
'''

while True:
    sensorsValue = alphabot.TRSensors_readLine(sensor=0)
    correction = 0

    for i in range(0, 5):
        sensors = 1 - (sensorsValue[i] - blackValue[i]) / (whiteValue[i] - blackValue[i])

        correction += coefficient[i] * sensors

    alphabot.setMotors(left=speed - correction, right=speed + correction)

    if vl53l0x.getRangeMillimeters() / 10 < 5:
        alphabot.stop()







