import math

import machine
import utime
from ble_peripheral import DrivePeripheral

from stm32_alphabot_v2 import AlphaBot_v2
alphabot = AlphaBot_v2()

drv = DriverPeripheral(name="jawabot")
# Buzzer on D0
d0 = machine.Pin('D2', machine.Pin.OUT)

def pitch (pin, noteFrequency, noteDuration, silence_ms = 10):
  if noteFrequency is not 0:
    microsecondsPerWave = 1e6 / noteFrequency
    millisecondsPerCycle = 1000 / (microsecondsPerWave * 2)
    loopTime = noteDuration * millisecondsPerCycle
    for x in range(loopTime):
      alphabot.controlBuzzer(0)
      utime.sleep_us(round(microsecondsPerWave))
      alphabot.controlBuzzer(1)
      utime.sleep_us(round(microsecondsPerWave))
  else:
    utime.sleep_ms(round(noteDuration))
  utime.sleep_ms(round(silence_ms))



def buzzer_playNotes (pin, notes, bpm = 120, ticks = 4):
  NOTE_FREQUENCIES = {
    'c': 16.352,
    'c#': 17.324, 'db': 17.324,
    'd': 18.354,
    'd#': 19.445, 'eb': 19.445,
    'e': 20.602,
    'f': 21.827,
    'f#': 23.125, 'gb': 23.125,
    'g': 24.500,
    'g#': 25.957, 'ab': 25.957,
    'a': 27.500,
    'a#': 29.135, 'bb': 29.135,
    'b': 30.868,
    'r': 0
  }
  for i in range(len(notes)):
    timeout = 60000 / bpm / ticks
    pin.off()
    n = notes[i].lower()
    data = n.split(':')
    note = 'r'
    octave = 4
    if len(data[0]) > 0:
      lastChar = data[0][-1]
      try:
        octave = int(lastChar)
        note = data[0].replace(lastChar, '')
      except:
        note = data[0]
    noteTicks = 1
    if len(data) > 1: noteTicks = int(data[1])
    n = {
      'note': note,
      'octave': octave,
      'ticks': noteTicks
    }
    n['f'] = NOTE_FREQUENCIES[n['note']]
    for o in range(n['octave']):
      n['f'] = n['f'] * 2
    pitch(pin, n['f'], timeout*n['ticks'])

buzzer_playNotes(d0, ['d', 'd', 'd', 'g:4', 'd#5:4', 'r1:2', 'c#5', 'c5', 'a#', 'a#5:4', 'd#5:2', 'r1', 'c#5', 'c5', 'a#', 'a#5:4', 'd#5:2', 'c#5', 'c5', 'c#5', 'a#:5'])

drv.send_message(recipient="CUSTOM", msg="Star Wars")