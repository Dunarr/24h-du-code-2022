import stm32_alphabot_v2

def turn (degre):

    alphabot = AlphaBot_v2()

    if degre == "180":
        alphabot.setMotors(right=15,left=-15)
        time.sleep(1.3)
        alphabot.moveForward(10)
        time.sleep(1)
    elif degre == "90":
        alphabot.setMotors(right=-15,left=15)
        time.sleep(0.7)
        alphabot.moveForward(10)
        time.sleep(1)
    elif degre == "-90":
        alphabot.setMotors(right=15,left=-15)
        time.sleep(0.7)
        alphabot.moveForward(10)
        time.sleep(1)
    elif degre == "-60":
        alphabot.setMotors(right=-15,left=15)
        time.sleep(0.6)
        alphabot.moveForward(10)
        time.sleep(1)
    elif degre == "60":
        alphabot.setMotors(right=15,left=-15)
        time.sleep(0.6)
        alphabot.moveForward(10)
        time.sleep(1)
    elif degre == "120":
        alphabot.setMotors(right=-15,left=15)
        time.sleep(0.9)
        alphabot.moveForward(10)
        time.sleep(1)
    elif degre == "-120":
        alphabot.setMotors(right=15,left=-15)
        time.sleep(0.9)
        alphabot.moveForward(10)
        time.sleep(1)