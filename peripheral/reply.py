import time
import random
import bluetooth
from ble_peripheral import DrivePeripheral


def randStr(length: int = 10):
    """Generate a random string of characters
    :param length: number of characters"""
    allowedChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    out = ''
    for _ in range(length):
        out += allowedChar[random.randrange(len(allowedChar))]
    return out


# -------------------------------
#             MAIN
# -------------------------------

drv = DrivePeripheral(name='jawabot')
print("Peripheral up and running")
msg_len = 1
while True:
    msg = drv.get_message(recipient='CUSTOM')
    # msg = drv.get_message(recipient='ST')
    print('received', len(msg), msg[:60])
    # time.sleep(1)
    drv.send_message(recipient='CUSTOM', msg=msg)
    # drv.send_message(recipient='ST', msg=randStr(msg_len))
    msg_len += 1


# def attendre_epreuve():
# def epreuvemystere():
# def sudoku():
# def mastermind():
