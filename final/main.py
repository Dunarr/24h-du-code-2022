from ble_peripheral import DrivePeripheral
from moove import avance, tourne
from mastermind import resolve_mastermind
from music import resolve_music
from sudoku import resolve_sudoku


recipient_main = 'CUSTOM'
recipient_team = 'CUSTOM'

prev_point = 0
current_point = 0
drv = DrivePeripheral(name='séraphor')


path = drv.get_message(recipient=recipient_main)
points = drv.get_message(recipient=recipient_team)

paths = path.split("\n")

for i in range(1, len(points)):
    avance()
    msg = drv.get_message(recipient=recipient_main)
    [name, data] = msg.split(">", 1)
    if name == "<MYSTERE":
        print("Je résoue le mystere")
        resolve_music(drv, recipient_main);
    elif name == "<SUDOKU":
        print("Je résoue le sudoku")
        resolve_sudoku(drv, recipient_main, data)
    elif name == "<MASTERMIND":
        print("Je résoue le mastermind")
        resolve_mastermind(drv, recipient_main, recipient_team, data)
    next_point = points[i]
    for line in paths:
        if line[0] == prev_point and line[2] == current_point and line[4] == next_point:
            angle = line.split(": ")[1]
            tourne(angle)

    prev_point = current_point
    current_point = next_point
