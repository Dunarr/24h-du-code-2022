def resolve_mastermind(drv, receipient1, receipient2, data):
    drv.send_message(recipient=receipient2, msg=data)
    resp = drv.send_message(recipient=receipient2)
    drv.send_message(recipient=receipient1, msg=resp)
